from cv2.typing import MatLike

import imutils

def resize_frame(frame: MatLike, size = 480) -> MatLike:
    return imutils.resize(frame, width=size) if frame.shape[1] > frame.shape[0] else imutils.resize(frame, height=size)
