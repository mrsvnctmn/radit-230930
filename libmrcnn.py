from maskrcnn_benchmark.config import cfg
from predictor import COCODemo

class Args:
    def __init__(self, config_file: str) -> None:
        self.config_file = config_file
        self.confidence_threshold = 0.7
        self.min_image_size = 224
        self.show_mask_heatmaps = False
        self.masks_per_dim = 1
        self.opts = []

class Mrcnn:
    def __init__(self, model: str) -> None:
        args = Args(model)

        cfg.merge_from_file(args.config_file)
        cfg.merge_from_list(args.opts)
        cfg.freeze()

        self.coco_demo = COCODemo(
            cfg,
            confidence_threshold=args.confidence_threshold,
            show_mask_heatmaps=args.show_mask_heatmaps,
            masks_per_dim=args.masks_per_dim,
            min_image_size=args.min_image_size
        )

    def process(self, frame):
        return self.coco_demo.run_on_opencv_image(frame)