import argparse
import cv2 as cv
import imutils as im
import time

from maskrcnn_benchmark.config import cfg
from predictor import COCODemo
from utility import resize_frame

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Webcam Demo")
    parser.add_argument(
        "--config-file",
        default="./maskrcnn-benchmark/configs/caffe2/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.7,
        help="Minimum score for the prediction to be shown",
    )
    parser.add_argument(
        "--min-image-size",
        type=int,
        default=224,
        help="Smallest size of the image to feed to the model. "
            "Model was trained with 800, which gives best results",
    )
    parser.add_argument(
        "--show-mask-heatmaps",
        dest="show_mask_heatmaps",
        help="Show a heatmap probability for the top masks-per-dim masks",
        action="store_true",
    )
    parser.add_argument(
        "--masks-per-dim",
        type=int,
        default=2,
        help="Number of heatmaps per dimension to show",
    )
    parser.add_argument(
        "opts",
        help="Modify model config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    parser.add_argument('--input', help="video path", default="./test/img_smol.jpg")


    args = parser.parse_args()

    print("Using config: {}".format(args.config_file))
    print("Using options: {}".format(args.opts))
    
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    cfg.freeze()

    coco_demo = COCODemo(
        cfg,
        confidence_threshold=args.confidence_threshold,
        show_mask_heatmaps=args.show_mask_heatmaps,
        masks_per_dim=args.masks_per_dim,
        min_image_size=args.min_image_size,
    )

    img = cv.imread(cv.samples.findFileOrKeep(args.input))
    img = resize_frame(img)

    start = time.time()
    composite, mk = coco_demo.run_on_opencv_image(img)
    end = time.time()

    # print(f"mkdim: {mk.shape}")

    mk = mk.squeeze(1).unsqueeze(-1)

    print(f"dim: {mk.shape}")

    for i, k in enumerate(mk):
        k = (k * 255).byte().cpu().numpy()
        k = k[:,:,0]

        cv.imshow(f"M{i}", k)

    print("Time: {:.2f} s / img".format(end - start))

    cv.imshow("Detections", composite)

    cv.waitKey()

    cv.destroyAllWindows()
