import cv2 as cv
import numpy as np

import cv2
import numpy as np
from typing import List

def segmentize_blobs(binary_mask: np.ndarray) -> List[np.ndarray]:
    # Ensure the input is a binary image
    if len(binary_mask.shape) != 2 or binary_mask.dtype != np.uint8:
        raise ValueError("Input mask must be a binary image (single channel, uint8 type).")

    # Find contours in the binary image
    contours, _ = cv2.findContours(binary_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Create a list to store each blob mask
    blob_masks = []

    # Iterate through each contour found
    for i, contour in enumerate(contours):
        # Create an empty mask for the current blob
        blob_mask = np.zeros_like(binary_mask)
        
        # Draw the contour on the empty mask
        cv.drawContours(blob_mask, [contour], -1, color=255, thickness=cv2.FILLED)
        
        # Append the mask to the list
        blob_masks.append(blob_mask)

    return blob_masks

# Example usage:
# binary_mask = cv2.imread('path_to_binary_mask.png', cv2.IMREAD_GRAYSCALE)
# blobs = segmentize_blobs(binary_mask)
# for i, blob in enumerate(blobs):
#     cv2.imwrite(f'blob_{i}.png', blob)

def pmatch(mask1: np.ndarray, mask2: np.ndarray) -> float:
    intersection = np.logical_and(mask1, mask2)
    union = np.logical_or(mask1, mask2)
    iou = np.sum(intersection) / np.sum(union)
    return iou * 100.0

if __name__ == "__main__":
    img = cv.imread("C:/Users/root/Desktop/void/blobs.png", cv.IMREAD_GRAYSCALE)
    img2 = cv.imread("C:/Users/root/Desktop/void/blobs.png", cv.IMREAD_GRAYSCALE)

    blobs = segmentize_blobs(img)
    blobs2 = segmentize_blobs(img2)

    for i, blob1 in enumerate(blobs):
        for j, blob2 in enumerate(blobs2):
            cv2.imwrite(f'blob1_{i}.png', blob1)
            cv2.imwrite(f'blob2_{j}.png', blob2)
            iou = pmatch(blob1, blob2)
            print(f"1.{i}|2.{j} = {iou}")

    cv.waitKey(0)

    cv.destroyAllWindows()