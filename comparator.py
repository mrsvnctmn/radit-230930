from cv2.typing import MatLike, Rect
from os import path
from torch import Tensor
from typing import List, Tuple

import cv2 as cv
import numpy as np
import sys

sys.path.append(path.abspath("./yola"))

def get_blob_bounding_box(blob_mask: MatLike) -> Rect:
    # Find contours in the blob mask
    contours, _ = cv.findContours(blob_mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    # Get the bounding box for the first contour (assuming there's only one)
    return cv.boundingRect(contours[0])

def pmatch(mask1: MatLike, mask2: MatLike) -> float:
    intersection = mask1 & mask2
    union = mask1 | mask2
    iou = np.sum(intersection) / np.sum(union)
    return iou * 100.0

def segmentize_blobs(binary_mask: MatLike, min_blob_area: int = 100) -> List[MatLike]:
    # Ensure the input is a binary image
    if len(binary_mask.shape) != 2 or binary_mask.dtype != np.uint8: #type: ignore
        raise ValueError("Input mask must be a binary image (single channel, uint8 type).")
    
    # Morphological operations to clean up the image
    kernel = np.ones((3, 3), np.uint8) #type: ignore
    cleaned_mask = cv.morphologyEx(binary_mask, cv.MORPH_CLOSE, kernel, iterations=2)
    cleaned_mask = cv.morphologyEx(cleaned_mask, cv.MORPH_OPEN, kernel, iterations=2)

    # Find contours in the cleaned binary image
    contours, _ = cv.findContours(cleaned_mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

    # Create a list to store each significant blob mask
    blob_masks = []

    # Iterate through each contour found
    for i, contour in enumerate(contours):
        area = cv.contourArea(contour)
        if area >= min_blob_area:
            # Create an empty mask for the current blob
            blob_mask = np.zeros_like(binary_mask)
            
            # Draw the contour on the empty mask
            cv.drawContours(blob_mask, [contour], -1, color=255, thickness=cv.FILLED) #type: ignore
            
            # Append the mask to the list
            blob_masks.append(blob_mask)

    return blob_masks
