feat.
1. dual backbackground model for stationary object detection (2/10/23)
2. separate mrcnn script, `mrcnn.py`. Slow, +2s per frame. (29/11/23)

Yang dibutuhkan:
1. python

Cara jalanin:
1. download code (sebelah kanan tombol "Clone", download zip)
2. extract
3. buka folder pake command line/terminal
4. install reqs.: `pip install -r requirements.txt`
5. run script:
    * `python main.py`: proses file default (`test/vtest.avi`)
    * `python main.py --input FILE`: buka file lain.
6. exit program: tombol `esc`

FAQ:
1. cara kerja?
    * Dual-BG Model for Stat. Obj. Det.: persis di papernya yang bahas dual background modelling.
    * M-RCNN: Belom diintegrasiin sama program utama. basic obj. detection dri contoh2 di internet. Pake model MRCNN tensorflow yang ditrain pake dataset COCO.