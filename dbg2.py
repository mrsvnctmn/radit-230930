import argparse
import cv2
import numpy as np

from utility import resize_frame

def main():
    parser = argparse.ArgumentParser("Test")
    parser.add_argument('--input', help="video path", default="test/vtest1.avi")
    args = parser.parse_args()

    # Open video capture
    cap = cv2.VideoCapture(cv2.samples.findFileOrKeep(args.input))

    # Create two background subtractors with different learning rates
    fgbg_short_term = cv2.createBackgroundSubtractorKNN(history=50, dist2Threshold=1600, detectShadows=False)
    fgbg_long_term = cv2.createBackgroundSubtractorKNN(history=500, dist2Threshold=1600, detectShadows=False)

    # Different learning rates
    learning_rate_short_term = -1
    learning_rate_long_term = -1

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        frame = resize_frame(frame)

        # Apply the short-term background subtractor
        # fgmask_short_term = fgbg_short_term.apply(frame, learningRate=learning_rate_short_term)
        fgmask_short_term = fgbg_short_term.apply(frame)

        # Apply the long-term background subtractor
        # fgmask_long_term = fgbg_long_term.apply(frame, learningRate=learning_rate_long_term)
        fgmask_long_term = fgbg_long_term.apply(frame)

        # Subtract the short-term mask from the long-term mask to get the static object mask
        static_object_mask = cv2.subtract(fgmask_long_term, fgmask_short_term)

        # Optional: Morphological operations to clean up the mask
        kernel = np.ones((3,3),np.uint8)
        static_object_mask = cv2.morphologyEx(static_object_mask, cv2.MORPH_OPEN, kernel)
        static_object_mask = cv2.morphologyEx(static_object_mask, cv2.MORPH_CLOSE, kernel)

        # Display the results
        cv2.imshow('Frame', frame)
        cv2.imshow('Short Term Foreground Mask', fgmask_short_term)
        cv2.imshow('Long Term Foreground Mask', fgmask_long_term)
        cv2.imshow('Static Object Mask', static_object_mask)

        if cv2.waitKey(5) & 0xFF == 27:  # Press 'Esc' to exit
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
