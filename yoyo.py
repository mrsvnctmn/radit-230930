from libyolact import Yola
from time import time
from utility import resize_frame

import argparse
import cv2 as cv
import torch
import torch.backends.cudnn as cudnn

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PyTorch Object Detection Webcam Demo")
    parser.add_argument('--input', help="video path", default="./test/img_smol.jpg")
    parser.add_argument('--yolact_model', help="YOLACT model file", dest="yolact_model", default="yola/weights/yolact_base_54_800000.pth")

    args = parser.parse_args()

    cudnn.fastest = True #type: ignore
    torch.set_default_tensor_type('torch.cuda.FloatTensor')

    detector = Yola(args.yolact_model)

    img = cv.imread(cv.samples.findFileOrKeep(args.input))
    # img = resize_frame(img, size=720)

    start = time()
    sovf, sovf_masks = detector.process(img)
    end = time()

    print(f"time: {end-start:.2f}")

    cv.imshow("Output", sovf)

    cv.waitKey()

    cv.destroyAllWindows()

