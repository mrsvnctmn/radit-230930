import argparse
import cv2 as cv
import imutils as im

from dualbg import DualBackgroundDetector, draw_bounding_box
from libmrcnn import Mrcnn
from time import time
from utility import resize_frame

FONT_SIZE = 0.35

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Test")
    parser.add_argument('--input', help="video path", default="test/vtest1.avi")
    parser.add_argument(
                '--mrcnn_config',
                help="MRCNN config file",
                dest="mrcnn_config",
                default="maskrcnn-benchmark/configs/caffe2/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml")
    args = parser.parse_args()

    cap = cv.VideoCapture(cv.samples.findFileOrKeep(args.input))

    ret, frame = cap.read()
    (h, w, _) = frame.shape
    frame = resize_frame(frame)
    (h, w, _) = frame.shape
    model = DualBackgroundDetector((h, w))

    if not cap.isOpened():
        raise FileNotFoundError("Someting's wrong...")
    else:
        ret, frame = cap.read()
        (h, w, _) = frame.shape

        detector = Mrcnn(args.mrcnn_config)

        while True:
            start = time()

            ret, frame = cap.read()
            
            if not ret:
                break

            frame = resize_frame(frame)
            
            (lf, sf, lb, sb, df, mask, bbox) = model.compute_masks(frame)
            trk, mask = detector.process(frame.copy())
            slb, _ = detector.process(lb.copy())

            det = draw_bounding_box(frame, bbox)

            # print(f"mask type {type(mask)}")
            print(f"mask shape {mask.shape}")

            end = time()
            try:
                fps = 1 / (end - start)
                if fps < 30:
                    cv.putText(det, f"{fps:.1f} FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (125, 0, 255), 1)
                else:
                    cv.putText(det, f"{fps:.1f} FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (125, 255, 0), 1)
            except:
                cv.putText(det, f"--- FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (0, 255, 0), 1)

            cv.imshow("VF", frame)
            cv.imshow("Det", det)
            cv.imshow("SOVF", trk)
            cv.imshow("SOLB", slb)
            cv.imshow("DF", df)

            if cv.waitKey(25) & 0xFF == 27: 
                break

            
