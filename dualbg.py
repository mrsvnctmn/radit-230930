import cv2 as cv
import imutils as im
import numpy as np

from cv2 import BackgroundSubtractorKNN as BGSub, createBackgroundSubtractorKNN as createBGSub
from cv2.typing import MatLike, Rect
from numpy import uint8 #type: ignore
from typing import List

# constants
BG_ZIV_LONG_LRATE = 0.0002
BG_ZIV_SHORT_LRATE = 0.002
BG_ZIV_LONG_HIST = 500
BG_ZIV_SHORT_HIST = 50
BG_ZIV_LONG_THRESH = 1600
BG_ZIV_SHORT_THRESH = 1600
AGG_RGB_MAX_E = 15
AGG_RGB_PENALTY = 7
BBOX_MIN_AREA = 100

def to_rgb(im: MatLike):
    h, w = im.shape
    ret = np.empty((h, w, 3), dtype=np.uint8) #type: ignore
    ret[:, :, 2] = ret[:, :, 1] = ret[:, :, 0] = im
    return ret

class DualBackgroundDetector:
    def __init__(self, image_shape):
        shape_rgb = (image_shape + (3,))

        self.current_frame = np.zeros(shape=shape_rgb, dtype=np.uint8) #type: ignore
        self.proposal_foreground = np.zeros(shape=shape_rgb, dtype=np.uint8) #type: ignore
        self.foreground_mask_long_term = np.zeros(shape=image_shape)
        self.foreground_mask_short_term = np.zeros(shape=image_shape)
        self.background_aggregator = np.zeros(shape=image_shape, dtype=np.int8) #type: ignore
        self.proposal_mask = np.zeros(shape=image_shape, dtype=np.uint8) #type: ignore

        self.f_bg_long = createBGSub(
            BG_ZIV_LONG_HIST, BG_ZIV_LONG_THRESH, False)
        self.f_bg_short = createBGSub(
            BG_ZIV_SHORT_HIST, BG_ZIV_SHORT_THRESH, False)

    def compute_masks(self, frame: MatLike):
        def compute(bgsub: BGSub, frame: MatLike, lrate: float):
            fg = np.zeros(shape=frame.shape, dtype=uint8)
            fg = bgsub.apply(frame, fg, lrate)
            bg = bgsub.getBackgroundImage()
            return np.where((fg == 0), 0, 1), bg

        def dilate(matrix: MatLike, kernel_size: int, kernel_type: int):
            img = matrix.astype(uint8)
            kernel = cv.getStructuringElement(
                kernel_type, (kernel_size, kernel_size))
            return cv.morphologyEx(img, cv.MORPH_DILATE, kernel)

        def cut(image: MatLike, mask: MatLike):
            if len(image.shape) == 2 or image.shape[2] == 1:
                return image * mask  # type: ignore #type
            elif len(image.shape) == 3 and image.shape[2] == 3:
                return image * to_rgb(mask)  # type: ignore #type
            else:
                raise IndexError(
                    "image has the wrong number of channels (must have 1 or 3 channels")

        def get_boxes(image: MatLike) -> List[Rect]:
            bbox = []
            contours, _ = cv.findContours(
                image, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
            for contour in contours:
                if cv.contourArea(contour) > BBOX_MIN_AREA:
                    rect = cv.boundingRect(contour)
                    if rect not in bbox:
                        bbox.append(rect)
            return bbox

        (h, w, _) = frame.shape
        # frame = im.resize(frame, width=480) if w > h else im.resize(frame, height=480)
        # frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # frame = np.dstack([frame, frame, frame]) #type: ignore

        (lf, lb) = compute(self.f_bg_long, frame, BG_ZIV_LONG_LRATE)
        (sf, sb) = compute(self.f_bg_short, frame, BG_ZIV_SHORT_LRATE)

        lf = dilate(lf, 1, cv.MORPH_ELLIPSE) #type: ignore
        sf = dilate(sf, 1, cv.MORPH_ELLIPSE) #type: ignore

        dfc = lf * np.logical_not(sf) #type: ignore
        opps = np.logical_not(dfc) #type: ignore
        res = self.background_aggregator + dfc
        res -= opps * AGG_RGB_PENALTY
        self.background_aggregator = np.clip(res, 0, AGG_RGB_MAX_E)

        mask = np.where(self.background_aggregator == AGG_RGB_MAX_E, 255, 0)
        bbox = get_boxes(dfc.astype(uint8))
        df = cut(frame, mask) #type: ignore

        return lf, sf, lb, sb, to_rgb(mask), mask, bbox #type: ignore
    
def draw_bounding_box(image: MatLike, bbox: List[Rect]):
    frame = np.copy(image)
    (h, w, _) = frame.shape
    # frame = im.resize(frame, width=480) if w > h else im.resize(frame, height=480)

    for s in bbox:
        cv.rectangle(frame, (s[0], s[1]),
                        (s[0]+s[2], s[1]+s[3]), (0, 0, 255), 2)
    return frame