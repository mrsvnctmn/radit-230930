from dataclasses import dataclass
import cv2 as cv
import numpy as np
import os
import time

from cv2.typing import MatLike
from cv2.dnn import Net
from typing import List

CONFIDENCE = 0.5
THRESHOLD = 0.5

@dataclass
class MaskRCNN:
    net: Net
    classes: List[str]

    def __init__(self, model_dir: str):
        weights_path = os.path.sep.join([model_dir, "frozen_inference_graph.pb"])
        config_path = os.path.sep.join([model_dir, "mask_rcnn_inception_v2_coco_2018_01_28.pbtxt"])
        classes_path = os.path.sep.join([model_dir, "mscoco_labels.names"])

        self.net = cv.dnn.readNetFromTensorflow(weights_path, config_path)

        with open(classes_path, "rt") as f:
            self.classes = f.read().rstrip('\n').split('\n')

    def process(self, frame: MatLike):
        def draw(framed, class_id, conf, left, top, right, bottom, class_mask):
            cv.rectangle(framed, (left, top), (right, bottom), (255, 178, 50), 3)

            label = "{:.2f}".format(conf)
            if self.classes:
                assert(class_id < len(self.classes))
                label = "{}:{}".format(self.classes[class_id], label)

            label_size, baseline = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.5, 1)
            top = max(top, label_size[1])
            cv.rectangle(framed, (left, top - round(1.5 * label_size[1])), (left + round(1.5 * label_size[0]), top + baseline), (255, 255, 255), cv.FILLED)
            cv.putText(framed, label, (left, top), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1)

            class_mask = cv.resize(class_mask, (right - left + 1, bottom - top + 1))
            mask = (class_mask > THRESHOLD)
            roi = framed[top:bottom + 1, left:right+1][mask]

            color = (255, 255, 255)

            framed[top:bottom + 1, left:right + 1][mask] = ([0.3*color[0], 0.3*color[1], 0.3*color[2]] + 0.7 * roi).astype(np.uint32) #type: ignore

            mask = mask.astype(np.uint8) #type: ignore
            contours, hierarcy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
            cv.drawContours(framed[top:bottom+1, left:right+1], contours, -1, color, 2, cv.LINE_8, hierarcy, 100)

        def post_process(res: MatLike, boxes: MatLike, masks: MatLike):
            num_classes = masks.shape[1]
            num_detections = boxes.shape[2]

            H, W = res.shape[:2]

            for i in range(num_detections):
                box = boxes[0, 0, i]
                mask = masks[i]
                conf = box[2]

                if conf > CONFIDENCE:
                    class_id = int(box[1])
                    
                    left = int(W * box[3])
                    top = int(H * box[4])
                    right = int(W * box[5])
                    bottom = int(H * box[6])
                    
                    left = max(0, min(left, W - 1))
                    top = max(0, min(top, H - 1))
                    right = max(0, min(right, W - 1))
                    bottom = max(0, min(bottom, H - 1))

                    class_mask = mask[class_id]

                    draw(res, class_id, conf, left, top, right, bottom, class_mask)

        result = frame.copy()

        blob = cv.dnn.blobFromImage(result, swapRB=True, crop=False)

        self.net.setInput(blob)

        start = time.time() # type: ignore
        (boxes, masks) = self.net.forward(["detection_out_final", "detection_masks"])
        post_process(result, boxes, masks)
        end = time.time() # type: ignore
        
        return result, (end - start)

if __name__ == "__main__":
    import argparse
    import imutils as im
    
    parser = argparse.ArgumentParser(description='Use this script to run Mask-RCNN object detection and segmentation')
    parser.add_argument('--input', help='Path to image file')
    args = parser.parse_args()

    model = MaskRCNN("model")

    img = cv.imread(cv.samples.findFileOrKeep(args.input))
    img = im.resize(img, width=720)

    res, time = model.process(img)

    print("[INFO] Mask R-CNN took {:.6f} seconds".format(time))

    cv.imshow("img", img)
    cv.imshow("res", res)

    cv.waitKey()