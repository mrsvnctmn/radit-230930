import argparse
import cv2 as cv
import numpy as np
import time
import torch
import torch.backends.cudnn as cudnn

from comparator import segmentize_blobs, get_blob_bounding_box, pmatch
from cv2.typing import Rect, MatLike
from dualbg import DualBackgroundDetector, draw_bounding_box
from enum import Enum
from libyolact import Yola
from libmrcnn import Mrcnn
from tqdm import tqdm
from utility import resize_frame

FONT_SIZE = 0.35
MATCH_THRESHOLD = 90.0

def concat_tile(im_list_2d):
    return cv.vconcat([cv.hconcat(im_list_h) for im_list_h in im_list_2d])

def blank(shape):
    return np.zeros(shape, dtype=np.uint8) #type: ignore

def mask_size(mask: MatLike):
    return np.sum((mask >= 128).astype(np.uint8)) #type: ignore  

def crop(bbox: Rect, frame: MatLike) -> MatLike:
    x, y, w, h = bbox
    return frame[y:y+h, x:x+w]

class Comparison(Enum):
    SAME = "same"
    BIGGER = "bigger"
    SMALLER = "smaller"

def compare_with_threshold(val0, val1, threshold: float):
    margin = val0 * (threshold / 100.0)
    
    if abs(val0 - val1) <= margin:
        return Comparison.SAME
    elif val1 > val0:
        return Comparison.BIGGER
    else:
        return Comparison.SMALLER

if __name__ == "__main__":
    # Program command argument parser

    parser = argparse.ArgumentParser(
        description="Dual-BG + Mask R-CNN for abandoned/stolen object detection")
    parser.add_argument(
        '--input', help='Path to video file to process', default='test/vtest1.avi')
    parser.add_argument(
        '--output', help='Path to video file to process', default='output.mp4')
    parser.add_argument(
        '--detection', help="Detection model (yolact/mrcnn)", default="yolact")
    parser.add_argument(
        '--yolact_model', help="YOLACT model file", dest="yolact_model", default="yola/weights/yolact_base_54_800000.pth")
    parser.add_argument(
        '--mrcnn_config', help="MRCNN config file", dest="mrcnn_config", default="maskrcnn-benchmark/configs/caffe2/e2e_mask_rcnn_R_50_FPN_1x_caffe2.yaml")
    args = parser.parse_args()

    # open video
    cap = cv.VideoCapture(cv.samples.findFileOrKeep(args.input))

    # if video not opened succcesfully
    if not cap.isOpened():
        raise FileNotFoundError(f"Something's wrong in opening {args.input}")
    else:
        # select and initialize segmentation method
        if args.detection == "yolact":
            print("Using YOLACT")

            cudnn.fastest = True #type: ignore
            torch.set_default_tensor_type('torch.cuda.FloatTensor')

            detector = Yola(args.yolact_model)
        elif args.detection == "mrcnn":
            print("Using MRCNN")
            detector = Mrcnn(args.mrcnn_config)
        else:
            raise AttributeError(f"Invalid detection backend: '{args.detection}'")

        # read first frame from video
        ret, frame = cap.read()

        # get frame shape
        (h, w, _) = frame.shape
        
        # resize frame (default to 480px)
        frame = resize_frame(frame)
        
        # get new size
        (h, w, _) = frame.shape
        
        # init dual background model with new size
        model = DualBackgroundDetector((h, w))

        frame_count = int(cap.get(cv.CAP_PROP_FRAME_COUNT))

        pbar = tqdm(total=frame_count, desc="Processing", unit="frames")

        BLANK = blank((h, w, 3))

        out = cv.VideoWriter(args.output, cv.VideoWriter_fourcc(*"avc1"), cap.get(cv.CAP_PROP_FPS), (3 * w, 3 * h)) #type: ignore

        # loop forever
        while True:
            # time (fps) counter start
            start = time.time()

            # read next frame from video
            ret, frame = cap.read()
            
            # break loop if there's no more frame
            if frame is None:
                break
            
            # resize captured frame (default 480px as with to dualbg model)
            frame = resize_frame(frame)

            # get dualbg masks
            (lf, sf, lb, sb, df, mask, bbox) = model.compute_masks(frame)

            # draw df bounding boxes over VF
            det = draw_bounding_box(frame, bbox)

            # convert to mask data type to u8
            masked = cv.convertScaleAbs(mask) #type:ignore

            # segmentize each blob of df
            csodf = segmentize_blobs(masked) # segmentize mask blobs #List[[h, w]]

            # calculate sovf & solb
            sovf, sovf_masks = detector.process(frame.copy()) #[h, w, 3], [N, h, w, 1]
            solb, solb_masks = detector.process(lb.copy()) #[h, w, 3], [N, h, w, 1]

            # if masks is none, create a blank mask
            sovf_masks_s = sovf_masks if sovf_masks is not None else torch.zeros((1, h, w, 1)) #[N, h, w, 1]
            solb_masks_s = solb_masks if solb_masks is not None else torch.zeros((1, h, w, 1)) #[N, h, w, 1]

            # copy frame    
            rslt = frame.copy() #[h, w, 3]
            dfvf = frame.copy() #[h, w, 3]
            vflb = frame.copy() #[h, w, 3]

            # for each mask in sovf
            for i, x in enumerate(sovf_masks_s):
                # convert
                ovf = (x * 255).byte().cpu().numpy()
                ovf = ovf[:,:,0]
                
                # for each mask in csodf
                for j, s in enumerate(csodf):
                    # match compare a mask in sovf with one in csodf
                    ff = pmatch(ovf, s) 

                    # if matchscore > threshold
                    if ff > MATCH_THRESHOLD:
                        # create bounding box aginst sovf mask
                        bbx = get_blob_bounding_box(ovf)

                        # get bbx dimension
                        x, y, w, h = bbx

                        # draw bbx
                        cv.rectangle(dfvf, (x, y), (x+w, y+h), (255, 0, 0), 1)

                        # draw matchscore
                        font = cv.FONT_HERSHEY_SIMPLEX
                        font_scale = 0.5
                        text = f'{ff:.1f}%'
                        text_size = cv.getTextSize(text, font, font_scale, 1)[0]
                        text_color = (255, 255, 255)  # White color
                        text_thickness = 1
                        cv.rectangle(dfvf, (x, y + text_size[1]), (x + text_size[0], y), (255, 0, 0), cv.FILLED)
                        cv.putText(dfvf, text, (x, y + 5), font, font_scale, text_color, text_thickness)

            for i, x in enumerate(sovf_masks_s):
                ovf = (x * 255).byte().cpu().numpy()
                ovf = ovf[:,:,0]

                for j, s in enumerate(csodf):
                    ff = pmatch(ovf, s)

                    if ff > MATCH_THRESHOLD:
                        bbx = get_blob_bounding_box(ovf)

                        x, y, w, h = bbx

                        cv.rectangle(vflb, (x, y), (x+w, y+h), (0, 255, 0), 1)

                        font = cv.FONT_HERSHEY_SIMPLEX
                        font_scale = 0.5
                        text = f'{ff:.1f}%'
                        text_size = cv.getTextSize(text, font, font_scale, 1)[0]
                        text_color = (0, 0, 0)  # White color
                        text_thickness = 1
                        cv.rectangle(vflb, (x, y + text_size[1]), (x + text_size[0], y), (0, 255, 0), cv.FILLED)
                        cv.putText(vflb, text, (x, y + 5), font, font_scale, text_color, text_thickness)

            # for each csodf
            for s in csodf:
                # bounding box of csodf
                csodf_bbx = get_blob_bounding_box(s)
                xc, yc, wc, hc = csodf_bbx
                csodf_crop = crop(csodf_bbx, s)
                csodf_crop_size = mask_size(csodf_crop)

                # num(sovf) & num(solb)
                num_sovf = sovf_masks_s.shape[0] if sovf_masks is not None else 0
                num_solb = solb_masks_s.shape[0] if solb_masks is not None else 0

                # list of sovf & losb crooped by csodf bbox
                cropped_sovf = [np.zeros((hc, wc), dtype=np.uint8)] #type: ignore
                cropped_solb = [np.zeros((hc, wc), dtype=np.uint8)] #type: ignore

                # for each sovf
                if num_sovf > 0:
                    for mvf in sovf_masks_s:
                        # transform torch Tensor -> numpy array
                        nmvf = (mvf * 255).byte().cpu().numpy()
                        
                        # reduce dimenstion: [h, w, 1] -> [h, w]
                        nmvf = nmvf[:,:,0]

                        # crop sovf to csodf
                        mvf_crop = crop(csodf_bbx, nmvf)

                        # add cropped sovf to list
                        cropped_sovf.append(mvf_crop)

                # for each solb, same as above
                if num_solb > 0:
                    for mlb in solb_masks_s:
                        nmlb = (mlb * 255).byte().cpu().numpy()
                        nmlb = nmlb[:,:,0]

                        mlb_crop = crop(csodf_bbx, nmlb)

                        cropped_solb.append(mlb_crop)

                # for each sovf/solb pair
                for ovf in cropped_sovf:
                    for olb in cropped_solb:
                        # mask sizes
                        ovf_size = mask_size(ovf)
                        olb_size = mask_size(olb)

                        # intial status
                        rclr = (0, 0, 0)
                        rtxt = "Undefined"

                        if num_sovf > 0:
                            csodf_sovf = pmatch(csodf_crop, ovf)

                            if csodf_sovf >= MATCH_THRESHOLD: #[1]
                                rtxt = "Abandoned"
                                rclr = (0, 255, 255)

                            else:
                                if num_solb > 0:
                                    sovf_solb = pmatch(ovf, olb)

                                    if sovf_solb >= MATCH_THRESHOLD: #[4]
                                        rtxt = "Ghost"
                                        rclr = (50, 255, 50)
                                    elif compare_with_threshold(ovf_size, olb_size, MATCH_THRESHOLD) == Comparison.BIGGER: #[3]
                                        rtxt = "Abandoned"
                                        rclr = (0, 255, 255)
                                    elif compare_with_threshold(ovf_size, olb_size, MATCH_THRESHOLD) == Comparison.SMALLER: #[5, 6]
                                        rtxt = "Stolen"
                                        rclr = (0, 0, 255)
                        else:
                            if num_solb > 0: #[8, 9]
                                rtxt = "Stolen"
                                rclr = (0, 0, 255)
                            else: #[7]
                                rtxt = "Ghost"
                                rclr = (50, 255, 50)


                        # drawing phase
                        cv.rectangle(rslt, (xc, yc), (xc + wc, yc + hc), rclr, 1)

                        font = cv.FONT_HERSHEY_SIMPLEX
                        font_scale = 0.5
                        text = rtxt
                        text_size = cv.getTextSize(text, font, font_scale, 1)[0]
                        text_color = (255, 255, 255)  # White color
                        text_thickness = 1
                        cv.rectangle(rslt, (xc, yc + text_size[1]), (xc + text_size[0], yc), rclr, cv.FILLED)
                        cv.putText(rslt, text, (xc, yc + 5), font, font_scale, text_color, text_thickness)

            # draw fps
            det = cv.rectangle(det, (25, 10), (105, 40), (0, 0, 0), -1)
            end = time.time()
            try:
                fps = 1 / (end - start)
                if fps < 30:
                    cv.putText(det, f"{fps:.1f} FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (0, 0, 255), 1)
                else:
                    cv.putText(det, f"{fps:.1f} FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (0, 255, 0), 1)
            except:
                cv.putText(det, f"--- FPS", (30, 30), cv.FONT_HERSHEY_SIMPLEX, FONT_SIZE, (0, 255, 0), 1)

            # display arrangement

            result = concat_tile([[frame, det, BLANK],
                                  [sovf, solb, df], 
                                  [dfvf, vflb, rslt]])

            out.write(result)

            pbar.update(1)

        out.release()

    cap.release()
